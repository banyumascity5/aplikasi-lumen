<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ayam goreng');
            $table->string('mie goreng');
            $table->string('ayam geprek');
            $table->string('gadu gadu');
            $table->string('nasi pecel');
            $table->string('lele');
            $table->string('nasi goreng');
            $table->string('rawon');
            $table->string('tempe penyet');
            $table->string('sup ayam');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
